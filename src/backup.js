const fs = require('fs');
const util = require('util');
const child_process = require('child_process');
const logger = require('../winston');
const moment = require('moment');
const beautify = require('json-beautify');
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');

const exec = util.promisify(child_process.exec);

const serviceAccount = require('../digital-learning-backups-firebase-adminsdk-gh58w-ecd1b46b16.json');

const app = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: 'digital-learning-backups.appspot.com'
});

const transport = nodemailer.createTransport({
    host: 'smtp-relay.gmail.com',
    port: 587
});

const MONGO_URI = process.env.MONGO_URI;
const DUMP_PATH = process.env.DUMP_PATH;

(async () => {
    await dumpDatabase();
    await saveBackupToFirebase();
    await deleteOldRemoteDumpFiles();
    await deleteOldLocalDumpFiles();
    process.exit(0);
})();

async function dumpDatabase() {
    

    if (!MONGO_URI) {
        logger.error(`no mongo uri provided as env variable. Ex: MONGO_URI=mongo://localhost:27017`);
        await sendMail(`no mongo uri provided as env variable. Ex: MONGO_URI=mongo://localhost:27017`);
        process.exit(1);
    }

    if (!DUMP_PATH) {
        logger.error(`dump location must be provided as env variable. Ex: DUMP_PAHT=/backup`);
        await sendMail(`dump location must be provided as env variable. Ex: DUMP_PAHT=/backup`);
        process.exit(1);
    }

    try {
        const currDate = moment();
        const fileName = `dump_digital_learning_${currDate.format('YYYY-MM-DD_HH-mm-ss')}.gz`;
        const shellCommand = `docker exec digital-learning_mongodb_1 sh -c 'mongodump --uri=${MONGO_URI} --archive --gzip' > ${DUMP_PATH}/${fileName}`;

        logger.info(`dump data base started`);
        logger.info(`${shellCommand}`);

        const { stdout, stderr } = await exec(shellCommand);

        if (stdout) { logger.info(stdout); }
        if (stderr) { logger.info(`mongo dump logs bellow \n${stderr}`); }

    } catch (error) {
        logger.error(`Error while dumping database \n${error.message}\n${error.stack}`);
        await sendMail(`Error while dumping database \n${error.message}\n${error.stack}`);
        process.exit(1);
    }

}

async function saveBackupToFirebase() {
    // Get last dump file name
    let fileName;

    try {
        const dumpFiles = fs.readdirSync(DUMP_PATH);

        if (dumpFiles.length === 0) { throw new Error(`no dump files in ${DUMP_PATH} location`) }

        fileName = dumpFiles.filter(elt => elt.startsWith('dump_digital_learning_')).sort().pop();

        if (!fileName) { throw new Error(`unable to get last dump file name`) }

    } catch (error) {
        logger.error(`Error while listing dump files \n${error.message}\n${error.stack}`);
        await sendMail(`Error while listing dump files \n${error.message}\n${error.stack}`);
        process.exit(1);
    }

    // Upload last dump file to firebase
    try {
        logger.info(`uploading ${fileName} to firebase`);
        const response = await app.storage().bucket().upload(`${DUMP_PATH}/${fileName}`);
        logger.info(`${fileName} uploaded successfully`);
    } catch (error) {
        logger.error(`Error while uploading ${fileName} firebase \n${error.message}\n${error.stack}`);
        await sendMail(`Error while uploading ${fileName} firebase \n${error.message}\n${error.stack}`);
        process.exit(1);
    }
}

async function deleteOldRemoteDumpFiles() {
    const deleteDate = moment().subtract(4, 'days').format('YYYY-MM-DD');
    try {
        logger.info(`start delete old dumps starting on ${deleteDate}`);
        await app.storage().bucket().deleteFiles({ prefix: `dump_digital_learning_${deleteDate}` });
        logger.info(`old dumps deleted from firebase \n \n`);
    } catch (error) {
        logger.error(`Error while deleting old dumps from firebase \n${error.message}\n${error.stack}`);
        await sendMail(`Error while deleting old dumps from firebase \n${error.message}\n${error.stack}`);
        process.exit(1);
    }
}

async function deleteOldLocalDumpFiles() {
    try {
        const dumpFiles = fs.readdirSync(DUMP_PATH);

        if (dumpFiles.length === 0) { throw new Error(`no dump files in ${DUMP_PATH} location`) }

        const filesToDelete = dumpFiles.filter(elt => elt.startsWith('dump_digital_learning_')).sort().slice(0, dumpFiles.length - 4);

        if (!filesToDelete) { return logger.info('no files to delete'); }

        logger.info(`start deleting ${filesToDelete.length} local files`);
        filesToDelete.forEach(file => fs.unlinkSync(`${DUMP_PATH}/${file}`));
        logger.info(`old local files deleted succsessfully`);

    } catch (error) {
        logger.error(`Error while deleting old local dumps \n${error.message}\n${error.stack}`);
        await sendMail(`Error while deleting old local dumps \n${error.message}\n${error.stack}`);
        process.exit(1);
    }
}

async function sendMail(mailContent) {
    const mailOptions = {
        from: 'noreply@londo-tech.com',
        to: `kevin.moutassi@londo.io, zachee.boum@londo.io, ronald.ayee@londo.io`,
        cc: `said.essiakou@londo.io, franck.libam@londo.io`,
        subject: `ERROR BACKUP DIGITAL LEARNING DATABASE`,
        text: `Backup Digital Learning database failed at ${moment().add(1, 'hour').format()} \n${mailContent}`
    };

    try {
        const info = await transport.sendMail(mailOptions);
        logger.info(`email notification sent \n${beautify(info)}`);
    } catch (error) {
        logger.error(`Error email notification \n${error.message}\n${error.stack}`);
    }
}