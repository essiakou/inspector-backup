const fs = require('fs');
const winston = require('winston');
const beautify = require('json-beautify');
const moment = require('moment');
const _ = require('lodash');
const { combine, timestamp, label, printf, metadata, colorize, errors } = winston.format;

const LOGS_DIR = `${__dirname}/logs`;

console.log(__dirname, LOGS_DIR)

if (!fs.existsSync(LOGS_DIR)) {
    fs.mkdirSync(LOGS_DIR);
}

function customFormat() {
    return combine(
        errors({ stack: true }),
        metadata({}),
        timestamp(),
        printf(({ level, message, time, meta }) => {
            let out = `[${moment(time).format('DD/MMM/YYYY:HH:mm:ss ZZ')}] [${level}] ${message}`;

            if (meta && meta.error) {
                out += meta.methodPath ? `\n${meta.methodPath}` : '';
                out += `\n${meta.stack}`;
            } else if (meta && meta.query) {
                out += `\n${meta.query}`;
            } else if (!_.isEmpty(meta)) {
                out += `\n${beautify(meta, null, 2, 80)}`;
            }

            return out;
        })
    )
}

const options = {
    file: {
        level: 'silly',
        filename: `${LOGS_DIR}/app.log`,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        handleExceptions: true,
        format: customFormat(),
        colorize: true,
    },
    console: {
        level: 'silly',
        format: customFormat(),
        handleExceptions: true,
        colorize: true
    }
}

const transports = [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
];

const logger = winston.createLogger({
    transports,
    exitOnError: false, // do not exit on handled exceptions
});

module.exports = logger;
